<?php
/**
 * PHPUnit
 *
 * Copyright (c) 2002-2008, Sebastian Bergmann <sb@sebastian-bergmann.de>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *
 *   * Neither the name of Sebastian Bergmann nor the names of his
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @category   Testing
 * @package    PHPUnit
 * @author     Sebastian Bergmann <sb@sebastian-bergmann.de>
 * @author     Jonathan H. Wage <jonwage@gmail.com>
 * @copyright  2002-2008 Sebastian Bergmann <sb@sebastian-bergmann.de>
 * @license    http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @link       http://www.phpunit.de/
 * @since      File available since Release 2.0.0
 */

require_once 'PHPUnit/Framework.php';

PHPUnit_Util_Filter::addFileToFilter(__FILE__, 'PHPUNIT');

/**
 * MockBrowserTestCase class to be used for functional testing using a PHP based MockBrowser
 *
 * @category   Testing
 * @package    PHPUnit
 * @author     Sebastian Bergmann <sb@sebastian-bergmann.de>
 * @author     Jonathan H. Wage <jonwage@gmail.com>
 * @copyright  2002-2008 Sebastian Bergmann <sb@sebastian-bergmann.de>
 * @license    http://www.opensource.org/licenses/bsd-license.php  BSD License
 * @version    Release: @package_version@
 * @link       http://www.phpunit.de/
 * @since      Class available since Release 2.0.0
 */
class PHPUnit_Extensions_MockBrowserTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * Path to the cookie file used by curl instances
     *
     * @var string
     */
    protected $curlCookieFile   = null;

    /**
     * Array of curl options to be used with each request
     *
     * @var array
     */
    protected $curlOptions        = array();

    /**
     * Array of the default curl options to always be used with each request
     *
     * @var array
     */
    protected $defaultCurlOptions = array(CURLOPT_RETURNTRANSFER  => true,
                                          CURLOPT_AUTOREFERER     => true,
                                          CURLOPT_FOLLOWLOCATION  => true,
                                          CURLOPT_FRESH_CONNECT   => true,
                                          CURLOPT_FORBID_REUSE    => true,
                                          CURLOPT_COOKIESESSION   => true);

    /**
     * The root web url for the mock browser
     *
     * @var string
     */
    protected $browserUrl       = null;

    /**
     * Array of the parsed url for the last executed request
     *
     * @var array
     */
    protected $parsedUrl        = array();

    /**
     * Array of request headers
     *
     * @var array
     */
    protected $headers          = array();

    /**
     * Array containing all the browsers history.
     * Used for back, forward and refresh functionality.
     *
     * @var array
     */
    protected $history          = array();

    /**
     * Current history position for the mock browser
     *
     * @var integer
     */
    protected $historyPosition  = -1;

    /**
     * Array of request/response information from the last executed request
     *
     * @var integer
     */
    protected $requestInfo      = array();

    /**
     * Array of response headers from the last executed request
     *
     * @var array
     */
    protected $responseHeaders  = array();

    /**
     * The raw text/html from the last executed request
     *
     * @var string
     */
    protected $responseText     = null;

    /**
     * The responseText parsed to a xml document
     *
     * @var object
     */
    protected $responseTextXml  = null;

    /**
     * The responseText parsed to a DomDocument
     *
     * @var object
     */
    protected $responseTextDom  = null;

    /**
     * Array of fields set. Used to submit forms
     *
     * @var array
     */
    protected $parameters       = array();

    /**
     * Browser method constants
     */
    const GET    = 'GET';
    const POST   = 'POST';
    const DELETE = 'DELETE';
    const PUT    = 'PUT';

    /**
     * Constructor. Overridden to set the path to the curl cookie file
     *
     * @param  string $filename
     * @param  string $options
     * @return void
     */
    public function __construct($filename, $options = array())
    {
        $this->curlCookieFile = sys_get_temp_dir() . 'PHPUnitMockBrowserCookies.txt';
        $this->initializeDefaultCurlOptions();

        parent::__construct($filename, $options);
    }

    /**
     * Tear down the mock browser test. Reset the browser history after each test.
     *
     * @return void
     */
    public function tearDown()
    {
        $this->reset();
        parent::tearDown();
    }

    /**
     * Browser action functions.
     *
     * Gives you the basic functions of a web browser to perform requests to make assertions on
     * for functional testing.
     */

    /**
     * Set the root browser url
     *
     * The data this function sets works with the getUrl() function. This method will accept a $uri
     * and it will be automatically appended to the $browserUrl and returned. This provides the ability
     * to have tests which have portable urls.
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $thisc->assertTitleEquals('My App');
     * </code>
     *
     * @see    getUrl
     * @param  string $browserUrl Url to set the browser to
     * @return void
     */
    public function setBrowserUrl($browserUrl)
    {
        $this->browserUrl = $browserUrl;
    }

    /**
     * Reset the mock browser entirely. Brings it to a fresh state.
     * Clears history, resets response/request information, clears cookies, etc.
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->reset();
     * </code>
     *
     * @return void
     */
    public function reset()
    {
        $this->browserUrl       = null;
        $this->parsedUrl        = array();
        $this->headers          = array();
        $this->history          = array();
        $this->historyPosition  = -1;
        $this->requestInfo      = null;
        $this->responseHeaders  = array();
        $this->responseText     = null;
        $this->initializeDefaultCurlOptions();
        $this->clearCookies();
    }

    /**
     * Perform a GET request
     *
     * You can optionally pass an array of $parameters or $headers to this function.
     * The $parameters will be automatically appended to the given $url as request parameters.
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->assertTitleEquals('My App');
     * $this->assertResponseContains('Welcome to my application');
     * </code>
     *
     * @param  string $url          Full url to GET to
     * @param  string $parameters   Array of parameters to send with the GET request
     * @return void
     */
    public function get($url, array $parameters = array())
    {
        return $this->request($url, self::GET, $parameters);
    }

    /**
     * Perform a POST request
     *
     * You can optionally specify an array of $parameters or $headers to this function
     * The $parameters will be automatically posted to the given $url
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->setParameter('username', 'jwage');
     * $this->setParameter('password', 'password');
     * $this->post($this->getUrl('login.php'));
     * $this->assertResponseIsRedirected(true);
     * $this->assertTitleEquals('My App - Logged In');
     * $this->assertResponseContains('You have successfully logged in');
     * </code>
     *
     * @param  string $url          Full url to POST to
     * @param  array  $parameters   Array of parameters to send with the post
     * @param  array  $headers      Array of http headers to send with the post
     * @return void
     */
    public function post($url, array $parameters = array())
    {
        return $this->request($url, self::POST, $parameters);
    }

    /**
     * Perform a PUT request
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->put($this->getUrl('index.php'));
     * </code>
     *
     * You can optionally specify an array of $parameters or $headers to this function
     * The $parameters and $headers will be sent with the request automatically.
     *
     * @param  string $url          Full url to PUT to
     * @param  array  $parameters   Array of parameters to send with the put
     * @return void
     */
    public function put($url, array $parameters = array())
    {
        return $this->request($url, self::PUT, $parameters);
    }

    /**
     * Perform a DELETE request
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->delete($this->getUrl('index.php'));
     * </code>
     *
     * @param  string $url          Full url to DELETE to
     * @param  array  $parameters   Array of parameters to send with the delete
     * @return void
     */

    public function delete($url, array $parameters = array())
    {
        return $this->request($url, self::DELETE, $parameters);
    }

    /**
     * Set a field value for a form which can be submitted later
     *
     * This function is intended to be used together with the native browser functions.
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->setParameter('username', 'jwage');
     * $this->setParameter('password', 'password');
     * $this->submitForm('Login');
     * $this->assertResponseIsRedirected(true);
     * $this->assertResponseContains('Login was successfull');
     * </code>
     *
     * @param  string $name     Name of the field to set
     * @param  string $value    Value of the field
     * @return void
     */
    public function setParameter($name, $value)
    {
        $this->parseArgumentAsArray($name, $value, $this->parameters);
    }

    /**
     * Convenience method for setting multiple fields with setParameter() from an array
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->setParameters(array('username' => 'jwage', 'password' => 'test'));
     * $this->submitForm('Login');
     * $this->assertResponseIsRedirected(true);
     * $this->assertResponseContains('Login was successfull');
     * </code>
     *
     * @param  array $fields    Array of key => value pairs to hand to setParameter()
     * @see    setParameter()
     * @return void
     */
    public function setParameters($fields)
    {
        foreach ($fields as $k => $v) {
            $this->parseArgumentAsArray($k, $v, $this->parameters);
        }
    }

    /**
     * Click a link present in the html from the last executed request
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->clickLink('Login');
     * $this->setParameter('username', 'jwage');
     * $this->setParameter('password', 'password');
     * $this->submitForm('Login');
     * $this->assertResponseIsRedirected(true);
     * $this->assertResponseContains('Login was successfull');
     * </code>
     *
     * @param  string $name         Name of the link to try and click
     * @return void
     * @throws Exception
     */
    public function clickLink($name)
    {
        $xPath = new DomXpath($this->responseTextDom);

        // text link where name is an attribute
        if ($link = $xPath->query(sprintf('//a[@*="%s"]', $name))->item(0)) {
            $url = $link->getAttribute('href');
        }

        // text link where name is the value
        if ($links = $xPath->query('//a[@href]')) {
            foreach($links as $link) {
                if(preg_replace(array('/\s{2,}/', '/\\r\\n|\\n|\\r/'), array(' ', ''), $link->nodeValue) == $name) {
                    $url = $link->getAttribute('href');
                }
            }
        }

        // image link where the name is the alt attribute value
        if ($link = $xPath->query(sprintf('//a/img[@alt="%s"]/ancestor::a', $name))->item(0)) {
            $url = $link->getAttribute('href');
        }

        if (isset($url) && $url) {
            $url = $this->fixRelativeUrl($url);

            return $this->get($url);
        }

        throw new Exception('Could not find link named "' . $name . '" to click');
    }

    /**
     * Submit a form present in the html from the last executed request
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->setParameter('username', 'jwage');
     * $this->setParameter('password', 'password');
     * $this->submitForm('Login');
     * $this->assertResponseIsRedirected(true);
     * $this->assertResponseContains('Login was successfull');
     * </code>
     *
     * @param  string $name         Name of the form submit button to click
     * @param  array  $parameters   Array of parameters to send with the submitted form
     * @return void
     */
    public function submitForm($name, array $parameters = array())
    {
        $xPath = new DomXpath($this->responseTextDom);
        $form = $form = $xPath->query(sprintf('//input[((@type="submit" or @type="button") and @value="%s") or (@type="image" and @alt="%s")]/ancestor::form', $name, $name))->item(0);

        // Make sure we have a form to submit
        if (!$form) {
            throw new Exception('Could not find form to submit');
        }

        $url = $form->getAttribute('action');
        $method = $form->getAttribute('method') ? strtoupper($form->getAttribute('method')):self::GET;
        $elements = $xPath->query('descendant::input | descendant::textarea | descendant::select', $form);

        $defaults = array();
        foreach ($elements as $element) {
            $elementName = $element->getAttribute('name');
            $elementType = $element->getAttribute('type');
            $elementValue = $element->getAttribute('value');
            $altValue = $element->getAttribute('alt');
            $nodeName = $element->nodeName;
            $value = null;

            if ($nodeName == 'input' && ($elementType == 'checkbox' || $elementType == 'radio')) {
                if ($element->getAttribute('checked')) {
                    $value = $elementValue;
                }
            } else if ($nodeName == 'input' && (($elementType != 'submit' && $elementType != 'button') || $elementValue == $name) && ($elementType != 'image' || $altValue == $name)) {
                $value = $elementValue;
            } else if ($nodeName == 'textarea') {
                $value = '';
                foreach ($element->childNodes as $el) {
                    $value .= $dom->saveXML($el);
                }
            } else if ($nodeName == 'select') {
                if ($multiple = $element->hasAttribute('multiple')) {
                    $elementName = str_replace('[]', '', $elementName);
                    $value = array();
                } else {
                    $value = null;
                }

                $found = false;
                foreach ($xPath->query('descendant::option', $element) as $option) {
                    if ($option->getAttribute('selected')) {
                        $found = true;
                        if ($multiple) {
                            $value[] = $option->getAttribute('value');
                        } else {
                            $value = $option->getAttribute('value');
                        }
                    }
                }

                if (!$found && !$multiple) {
                    $value = $xPath->query('descendant::option', $element)->item(0)->getAttribute('value');
                }
            }

            if ($value !== null) {
                $this->parseArgumentAsArray($elementName, $value, $defaults);
            }
        }

        $parameters = array_merge($defaults, $parameters);
        $url = $this->fixRelativeUrl($url);

        if ($method == self::POST) {
            return $this->post($url, $parameters);
        } else {
            return $this->get($url, $parameters);
        }
    }

    /**
     * Go back one position in the mock browser history
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->assertResponseContains('My App');
     * $this->get($this->getUrl('login.php'));
     * $this->assertResponseContains('Login');
     * $this->goBack();
     * $this->assertResponseContains('My App');
     * </code>
     *
     * @return void
     */
    public function goBack()
    {
        if ($this->historyPosition < 1) {
            throw new Exception('Cannot go back. You are already at the first position in the history.');
        }

        --$this->historyPosition;

        return $this->goToHistoryPosition($this->historyPosition);
    }

    /**
     * Go forward one position in the mock browser history
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->assertResponseContains('My App');
     * $this->get($this->getUrl('login.php'));
     * $this->assertResponseContains('Login');
     * $this->goBack();
     * $this->assertResponseContains('My App');
     * $this->goForward();
     * $this->assertResponseContains('Login');
     * </code>
     *
     * @return void
     */
    public function goForward()
    {
        if ($this->historyPosition > count($this->history) - 2) {
            throw new Exception('Cannot go forward. You are already at the last position in the history.');
        }

        ++$this->historyPosition;

        return $this->goToHistoryPosition($this->historyPosition);
    }

    /**
     * Refresh the current browser history position/url
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->assertResponseContains('View Count: 1');
     * $this->refresh();
     * $this->assertResponseContains('View Count: 2');
     * </code>
     *
     * @return void
     */
    public function refresh()
    {
        if ($this->historyPosition < 0) {
            throw new Exception('Nothing in history to refresh.');
        }

        return $this->goToHistoryPosition($this->historyPosition);
    }

    /**
     * Clear cookies from the browser
     *
     * @return void
     */
    public function clearCookies()
    {
        unlink($this->curlCookieFile);
    }

    /**
     * Set a request header to be sent with the next request
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->setHeader('Hello', 'World');
     * $this->get($this->getUrl('index.php'));
     * </code>
     *
     * @param  string $name  Name of the header to set
     * @param  string $value Value of the header
     * @return void
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }

    /**
     * Get absolute web url. This function combines a uri with the browserUrl set.
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->assertResponseContains('My App');
     * </code>
     *
     * @param  string $uri Uri to get the complete web url for
     * @return void
     */
    public function getUrl($uri = null)
    {
        $lastChar = $this->browserUrl[strlen($this->browserUrl) - 1];
        $url = $lastChar == '/' ? $this->browserUrl . $uri:$this->browserUrl . '/' . $uri;

        return $url;
    }

    /**
     * Get the absolute url for a relative url based on the url information from the last executed request
     *
     * <code>
     * $this->setBrowserUrl('http://localhost/folder/myapp');
     * $this->get($this->getUrl('index.php'));
     * $this->get($this->getAbsoluteUrl('folder/myapp/index.php'));
     * $this->assertResponseContains('My App');
     * </code>
     *
     * @param  string $uri
     * @return string $url
     */
    public function getAbsoluteUrl($uri = null)
    {
        if ($this->isRelativeUrl($uri)) {
            $firstChar = $uri[0];
            $url  = $this->parsedUrl['scheme'] . '://' . $this->parsedUrl['host'] . ':' . $this->parsedUrl['port'];
            $url .= $firstChar == '/' ? $uri:'/' . $uri;
        } else {
            throw new Exception('Uri passed is not a relative url: "' . $uri . '"');
        }

        return $url;
    }

    /**
     * Get the response text for the last executed request
     *
     * @return string $responseText
     */
    public function getResponseText()
    {
        return $this->responseText;
    }

    /**
     * Get the response text for the last executed request with html stripped
     *
     * @return string $responseTextNoHtml
     */
    public function getResponseTextNoHtml()
    {
        return $this->responseTextNoHtml;
    }

    /**
     * Browser Assertion Testing Functions
     *
     * All the methods below give you the ability to test certain things after executing browser requests.
     */

    /**
     * Assert page title is equal to passed string
     *
     * @param  string $title    Expected title of the page
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertTitleEquals($title, $message = 'Title does not equal passed text')
    {
        $this->assertEquals((string) $this->responseTextXml->head->title, $title, $message);
    }

    /**
     * Assert page title is not equal to passed string
     *
     * @param  string $title    Expected title of the page
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertTitleNotEquals($title, $message = 'Title equals passed text')
    {
        $this->assertNotEquals((string) $this->responseTextXml->head->title, $title, $message);
    }

    /**
     * Assert response text contains the passed string of text
     *
     * @param  string $text     String of text to look for on the page
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseContains($text, $message = 'Response does not contain the passed text')
    {
        $this->assertTrue(strpos($this->responseTextNoHtml, $text) !== FALSE, $message);
    }

    /**
     * Assert response text does not contain the passed string of text
     *
     * @param  string $text     String of text to look for on the page
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseNotContains($text, $message = 'Response contains the passed text')
    {
        $this->assertTrue(strpos($this->responseTextNoHtml, $text) === FALSE, $message);
    }

    /**
     * Assert the request response code is equal to the passed response code
     *
     * @param  integer $responseCode Integer response code to check for
     * @param  string  $messsage     Message to display if test fails
     * @return void
     */
    public function assertResponseCodeEquals($responseCode, $message = 'Response code equals the passed code')
    {
        $this->assertEquals($this->requestInfo['http_code'], $responseCode, $message);
    }

    /**
     * Assert the request response code is not equal to the passed response code
     *
     * @param  integer $responseCode Integer response code to check for
     * @param  string  $messsage     Message to display if test fails
     * @return void
     */
    public function assertResponseCodeNotEquals($responseCode, $message = 'Response code does not equal passed code')
    {
        $this->assertNotEquals($this->requestInfo['http_code'], $responseCode, $message);
    }

    /**
     * Assert that response code is one that is an error
     *
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseCodeIsError($message = 'Error code is not an error')
    {
        $error = $this->requestInfo['http_code'] == 400 || $this->requestInfo['http_code'] == 500;
        $this->assertTrue($error, $message);
    }

    /**
     * Assert that response code is not one that is an error
     *
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseCodeIsNotError($message = 'Error code is an error')
    {
        $error = $this->requestInfo['http_code'] == 400 || $this->requestInfo['http_code'] == 500;
        $this->assertFalse($error, $message);
    }

    /**
     * Assert that the browsers current url is equal to the url passed
     *
     * @param  string $url      Full url to compare against the browsers current url
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertCurrentUrlEquals($url, $message = 'Current url does not equal the passed url')
    {
        $this->assertEquals($this->requestInfo['url'], $url, $message);
    }

    /**
     * Assert that the browsers current url is not equal to the url passed
     *
     * @param  string $url      Full url to compare against the browsers current url
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertCurrentUrlNotEquals($url, $message = 'Current url equals the passed url')
    {
        $this->assertNotEquals($this->requestInfo['url'], $url, $message);
    }

    /**
     * Assert that a response header exists
     *
     * @param  string $name     Name of the response header to check for
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseHeaderExists($name, $message = 'Response header does not exist')
    {
        $this->assertTrue(isset($this->responseHeaders[$this->normalizeHeaderName($name)]), $message);
    }

    /**
     * Assert that a response header does not exist
     *
     * @param  string $name     Name of the response header to check for
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseHeaderNotExists($name, $message = 'Response header exists')
    {
        $this->assertFalse(isset($this->responseHeaders[$this->normalizeHeaderName($name)]), $message);
    }

    /**
     * Assert that a response header name equals the passed value
     *
     * @param  string $name     Name of the response header to check for
     * @param  string $value    Value of the response header to check for
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseHeaderEquals($name, $value, $message = 'Response header does not equal passed value')
    {
        $this->assertEquals($this->getResponseHeader($name), $value, $message);
    }

    /**
     * Assert that a response header name does not equal the passed value
     *
     * @param  string $name     Name of the response header to check for
     * @param  string $value    Value of the response header to check for
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseHeaderNotEquals($name, $value, $message = 'Response header equals passed value')
    {
        $this->assertNotEquals($this->getResponseHeader($name), $value, $message);
    }

    /**
     * Assert whether the last executed request was redirected or not
     *
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseIsRedirected($message = 'Response was not redirected')
    {
        $this->assertTrue($this->isRedirected(), $message);
    }

    /**
     * Assert whether the last executed request was redirected or not
     *
     * @param  string $messsage Message to display if test fails
     * @return void
     */
    public function assertResponseIsNotRedirected($message = 'Response was redirected')
    {
        $this->assertFalse($this->isRedirected(), $message);
    }

    /**
     * Protected Browser Methods
     *
     * All methods below are used internally by the MockBrowser and are not intended for public use
     * inside of test cases.
     */

    /**
     * Execute an http request via curl
     *
     * @param  string  $url             Url to make the request to
     * @param  string  $method          Method of the request(POST/GET/DELETE/PUT)
     * @param  array   $parameters      Parameters to send with the request
     * @param  boolean $addToHistory    True/false value for whether or not to add the request to the history
     * @return void
     */
    protected function request($url, $method = self::GET, array $parameters = array(), $addToHistory = true)
    {
        $this->resetResponse();
        $this->parseUrl($url);

        // Append array of parameters to url if request method is a GET
        if ($method == self::GET && !empty($parameters)) {
            $url .= strpos($url, '?') !== false ? '&':'?' . http_build_query($parameters, '', '&');
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HEADERFUNCTION, array($this, 'readResponseHeader'));
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_COOKIEJAR, $this->curlCookieFile);
        curl_setopt($curl, CURLOPT_COOKIEFILE, $this->curlCookieFile);

        foreach ($this->curlOptions as $name => $value) {
          curl_setopt($curl, $name, $value);
        }

        $headers = $this->getPreparedRequestHeaders($headers);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        if (isset($headers['Accept-Encoding'])) {
            curl_setopt($curl, CURLOPT_ENCODING, $headers['Accept-Encoding']);
        }

        $parameters = $this->getPreparedRequestParameters($parameters);

        if ($addToHistory) {
            $this->addToHistory($url, $method, $parameters, $headers);
        }

        if ($method == self::POST) {
            curl_setopt($curl, CURLOPT_POST, true);
        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, $parameters);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);

        $responseText = curl_exec($curl);

        if (curl_errno($curl)) {
          throw new Exception(curl_error($curl));
        }

        $this->requestInfo = curl_getinfo($curl);
        $this->setResponse($responseText);

        curl_close($curl);

        $this->parameters = array();

        return true;
    }

    /**
     * Initialize curl options with the array of default curl options
     *
     * @return void
     */
    protected function initializeDefaultCurlOptions()
    {
        $this->curlOptions = $this->defaultCurlOptions;
    }

    /**
     * Get array of prepared request headers to send with the curl request
     *
     * @param  array $parameters Array of parameters to prepare
     * @return array $parameters Array of prepared parameters
     * @author Jonathan Wage
     */
    protected function getPreparedRequestParameters(array $parameters = array())
    {
        // Merge any parameters collected from setParameter() calls in to the parameters to be sent with the request
        $parameters = array_merge($parameters, $this->parameters);

        return $parameters;
    }

    /**
     * Get the complete array of prepared headers to send with the curl request
     *
     * @param  array $headers Array of headers to prepare
     * @return array $headers Array of prepared headers
     */
    protected function getPreparedRequestHeaders(array $headers = array())
    {
        $headers = array_merge($this->headers, $headers);
        $headers = $this->normalizeHeaderNames($headers);

        return $headers;
    }

    /**
     * Set the response from curl_exec()
     *
     * Will decode encoding, parse in to xml and load a dom document
     *
     * @param  string $response
     * @return void
     */
    protected function setResponse($response)
    {
        $response = $this->decodeResponseEncoding($response);
        $this->responseTextXml = simplexml_load_string($response);
        $this->responseTextDom = DOMDocument::loadHTML($response);
        $this->responseText = $response;
        $this->responseTextNoHtml = strip_tags($response);
    }

    /**
     * Decode the response encoding based on the Content-Encoding response headers
     *
     * @param string $response
     * @return void
     */
    protected function decodeResponseEncoding($response)
    {
        $encoding = isset($this->responseHeaders['Content-Encoding']) ? strtolower($this->responseHeaders['Content-Encoding']):null;
        switch ($encoding) {
            case 'gzip':
                $responseText = gzinflate(substr($response, 10));
                break;
            case 'deflate':
                $responseText = gzuncompress($response);
                break;

            default:
                $responseText = $response;
                break;
        }

        return $responseText;
    }

    /**
     * Fix relative url.
     *
     * This function will automatically convert a relative url to an absolute url based on the current url
     * of the browser which is set by the last executed request.
     *
     * @param  string $url
     * @return string $url
     */
    protected function fixRelativeUrl($url)
    {
        if ($this->isRelativeUrl($url)) {
            $url = $this->getAbsoluteUrl($url);
        }

        return $url;
    }

    /**
     * Check if the passed url is relative or not
     *
     * @param  string  $url
     * @return boolean $isRelative
     */
    protected function isRelativeUrl($url)
    {
        $parsedUrl = parse_url($url);
        return !isset($parsedUrl['host']) ? true:false;
    }

    /**
     * Get value of a response header name
     *
     * @param  string $name         Name of response header to get
     * @return string $headerValue  Value of the response header
     */
    protected function getResponseHeader($name)
    {
        return $this->responseHeaders[$this->normalizeHeaderName($name)];
    }

    /**
     * Check if the last execute request was redirected or not
     *
     * @return boolean $success
     */
    protected function isRedirected()
    {
        return ($this->requestInfo['http_code'] == 302 || $this->requestInfo['http_code'] == 303);
    }

    /**
     * Reset the response information
     *
     * @return void
     */
    protected function resetResponse()
    {
        $this->requestInfo              = array();
        $this->responseHeaders          = array();
        $this->responseText             = null;
        $this->responseTextXml          = null;
        $this->responseTextDom          = null;
    }

    /**
     * Make a curl request for a specific history position.
     * This is used by goBack(), goForward() and refresh()
     *
     * @param  integer $historyPosition Integer history position to go to in the history array
     * @return void
     */
    protected function goToHistoryPosition($historyPosition)
    {
        return $this->request($this->history[$historyPosition]['uri'],
                              $this->history[$historyPosition]['method'],
                              $this->history[$historyPosition]['parameters'],
                              $this->history[$historyPosition]['headers'], false);
    }

    /**
     * Add a set of request information to the history
     *
     * @param  string $url          Url of the request
     * @param  string $method       Method of the request
     * @param  array  $parameters   Parameters for the request
     * @param  array  $headers      Headers for the request
     * @return void
     */
    protected function addToHistory($url, $method, array $parameters, array $headers)
    {
        $this->history = array_slice($this->history, 0, $this->historyPosition + 1);
        $this->history[] = array('uri'        => $url,
                                 'method'     => $method,
                                 'parameters' => $parameters,
                                 'headers'    => $headers);

        $this->historyPosition = count($this->history) - 1;
    }

    /**
     * Normalize the names of headers in an array of headers
     *
     * @param  array $headers               Array of headers to normalize and return
     * @return array $normalizedHeaders     Array of normalized headers
     */
    protected function normalizeHeaderNames($headers)
    {
        $normalizedHeaders = array();
        foreach ($headers as $name => $value) {
            if (!preg_match('/[a-z][A-Z]*/', $name)) {
                throw new Exception('Invalid header "' . $name . '"');
            }

            $normalizedHeaders[$this->normalizeHeaderName($name)] = trim($value);
        }

        return $normalizedHeaders;
    }

    /**
     * Normalize a single header name
     *
     * @param  string $name                 Name of the un-normalized header
     * @return string $normalizedHeaderName Name of the normalized header
     */
    protected function normalizeHeaderName($name)
    {
        return preg_replace('/\-(.)/e', "'-'.strtoupper('\\1')", strtr(ucfirst(strtolower($name)), '_', '-'));
    }

    /**
     * Parse http query request parameters/arguments to a php array
     *
     * @param  string $name   Name of the field
     * @param  string $value  Value of the field
     * @param  array  $vars   Array to parse the information in to
     * @return void
     */
    protected function parseArgumentAsArray($name, $value, &$vars)
    {
        if (false !== $pos = strpos($name, '[')) {
            $var = &$vars;
            $tmps = array_filter(preg_split('/(\[ | \[\] | \])/x', $name));
            foreach ($tmps as $tmp) {
                $var = &$var[$tmp];
            }

            if ($var) {
                if (!is_array($var)){
                    $var = array($var);
                }

                $var[] = $value;
            } else {
                $var = $value;
            }
        } else {
            $vars[$name] = $value;
        }
    }

    /**
     * Parse a url to an array of url information
     *
     * @param  string $url Full url to parse
     * @return array $parsedUrl
     */
    protected function parseUrl($url)
    {
        $parsedUrl = parse_url($url);
        $parsedUrl['query'] = isset($parsedUrl['query']) ? $parsedUrl['query']:'/';
        $parsedUrl['port'] = isset($parsedUrl['port']) ? $parsedUrl['port']:80;

        $this->parsedUrl = $parsedUrl;

        return $parsedUrl;
    }

    /**
     * Call back function for curl to read the headers in to a class variable
     *
     * @param  string $curl     Instance/resource for curl connection
     * @param  string $header   Header to be parsed and added to response headers array
     * @return integer $headerLength
     */
    protected function readResponseHeader($curl, $header)
    {
        $e = explode(':', $header);

        if (isset($e[1])) {
            $this->responseHeaders[$this->normalizeHeaderName($e[0])] = trim($e[1]);
        }

        return strlen($header);
    }
}