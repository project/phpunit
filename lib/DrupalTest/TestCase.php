<?php
/**
* @file
* Defines a Test Case for all Drupal Tests. 
*
* @package DrupalTest
* @author Christopher Hopper <christopher.jf.hopper@gmail.com>
*/

/**
* DrupalTest TestCase
* 
* Defines the fixtures that are required or assist when running 
* multiple Drupal tests using PHPUnit. 
* 
* @package DrupalTest
* @author  Christopher Hopper <christopher.jf.hopper@gmail.com>
*/
class DrupalTest_TestCase extends PHPUnit_Framework_TestCase {
  /**
  * A location where we can store the working directory we started in 
  * before we put ourselves in the root path of the drupal install. 
  * 
  * @var string
  */
  protected $currentWorkingDir;
  
  /**
  * Disable backup and restore operations for the global and super-
  * global variables. This prevents breaking references to MySQL-Link 
  * resources such as in the Drupal variable $GLOBALS['active_db']
  * 
  * @link http://sebastian-bergmann.de/archives/797-Global-Variables-and-PHPUnit.html 
  *   Global Variables and PHPUnit - Sebastian Bergmann 
  * @var boolean
  */
  protected $backupGlobals = FALSE;
  
  /**
   * Set up environment for a unit test case
   *
   * @return void
   */
  protected function setUp() {
    parent::setUp();

    // When using the command line test runner, put ourselves in the 
    // correct working directory for Drupal. 
    if (PHP_SAPI == 'cli') {
      $working_dir = getcwd();
      $drupal_root = DrupalTest_Bootstrap::getInstance()->drupalRoot;
      if ($working_dir != $drupal_root) {
        // Save the current working directory. 
        $this->currentWorkingDir = $working_dir;
        // Put us in the root path of the drupal install. 
        chdir($drupal_root);
      }
    }

    /** 
     * Supress devel footer output.
     * 
     * @global boolean devel_shutdown
     * @see devel_shutdown
     */
    $GLOBALS['devel_shutdown'] = FALSE;
  }

  /**
   * Tear down environment for a unit test case
   *
   * @return void
   */
  protected function tearDown() {
    parent::tearDown();

    // When using the command line test runner, we find a working 
    // directory has been saved, return us to that working directory 
    // that we started in. 
    if (PHP_SAPI == 'cli' && !empty($this->currentWorkingDir)) {
      chdir($this->currentWorkingDir);
    }
  }
}