<?php
/**
* @file
* Defines a Test Suite for all Drupal Tests. 
*
* @package DrupalTest
* @author Christopher Hopper <christopher.jf.hopper@gmail.com>
*/

/**
* DrupalTest TestSuite
* 
* Prepare and run a collection of Drupal Test Cases as a Suite. 
* 
* @package DrupalTest
* @author  Christopher Hopper <christopher.jf.hopper@gmail.com>
*/
class DrupalTest_TestSuite extends PHPUnit_Framework_TestSuite {
  /**
  * A location where we can store the working directory we started in 
  * before we put ourselves in the root path of the drupal install. 
  * 
  * @var string
  */
  protected $currentWorkingDir;
  
  /**
   * @return DrupalTest_TestSuite
   *    Instance of the DrupalTest TestSuite.
   */
  public static function suite() {
    return new DrupalTest_TestSuite('DrupalTest');
  }
  
  /**
   * Shared setUp() method for all Drupal test suites
   *
   * @return void
   */
  protected function setUp() {
    parent::setUp();

    // Back-up the Drupal Database prior to running Test Suite.
//    DrupalTest_Bootstrap::getInstance()->backupDatabase();

    // When using the command line test runner, put ourselves in the 
    // correct working directory for Drupal. 
    if (PHP_SAPI == 'cli') {
      $working_dir = getcwd();
      $drupal_root = DrupalTest_Bootstrap::getInstance()->drupalRoot;
      if ($working_dir != $drupal_root) {
        // Save the current working directory. 
        $this->currentWorkingDir = $working_dir;
        // Put us in the root path of the drupal install. 
        chdir($drupal_root);
      }
    }

    // Create a shared test_user fixture that will exist for all tests in 
    // the test suite.
    $test_user = array(
      'name'   => 'phpunit_testuser',
      'mail'   => 'phpunit_testuser@domain.com',
      'pass'   => 'password',
      'status' =>  1,
    );
    
    $this->test_user = user_load($test_user);
    if ($this->test_user == FALSE) {
      $this->test_user = user_save('', $test_user);
    }
  }

  /**
   * Shared tearDown() method for all Drupal test cases
   *
   * @return void
   */
  protected function tearDown() {
    parent::tearDown();
    
    // Remove the test user created for our shared fixture in setUp. 
    if (isset($this->test_user->uid)) {
      user_delete(array(), $this->test_user->uid);
    }
    
    // When using the command line test runner, we find a working 
    // directory has been saved, return us to that working directory we 
    // started in. 
    if (PHP_SAPI == 'cli' && !empty($this->currentWorkingDir)) {
      chdir($this->currentWorkingDir);
    }
    
    // Restore the Drupal Database back to the state saved prior to 
    // running our Test Suite.
//    DrupalTest_Bootstrap::getInstance()->restoreDatabase();
  }
}
 