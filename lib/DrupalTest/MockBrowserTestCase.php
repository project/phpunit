<?php
/**
 * DrupalTest Mock Browser Test Case
 *
 * This class provides a way to use a mock browser for functional testing on Drupal sites.
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class DrupalTest_MockBrowserTestCase extends PHPUnit_Extensions_MockBrowserTestCase
{
  /**
   * Set up environment for a mock browser test case
   *
   * @return void
   */
  public function setUp()
  {
    global $base_url;

    $this->setBrowserUrl($base_url);
    $this->get($this->getUrl());
    parent::setUp();
  }

  /**
   * Tear down environment for a mock browser test case
   *
   * @return void
   */
  public function tearDown()
  {
    parent::tearDown();
  }
}