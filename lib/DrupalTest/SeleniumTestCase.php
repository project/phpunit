<?php
/**
 * DrupalTest Selenium Test Case
 *
 * This class provides a way to use the Selenium RC server for functional testing on Drupal sites
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class DrupalTest_SeleniumTestCase extends PHPUnit_Extensions_SeleniumTestCase
{
  /**
   * Set up environment for selenium test case
   *
   * @return void
   */
  public function setUp()
  {
    global $base_url;

    $this->setBrowserUrl($base_url);
    $this->open($base_url);
    parent::setUp();
  }

  /**
   * Tear down environment for selenium test case
   *
   * @return void
   */
  public function tearDown()
  {
    parent::tearDown();
  }
}