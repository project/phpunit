<?php
/**
 * DrupalTest base class.
 *
 * This class provides auto loading for the DrupalTest lib. 
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class DrupalTest {
  /**
   * Autoload DrupalTest classes
   *
   * @return boolean $success
   */
  public static function autoload($className) {
    $path = dirname(__FILE__) . '/' . str_replace('_', '/', $className) . '.php';

    if (file_exists($path)) {
      require($path);
      return true;
    }

    return false;
  }
}
