<?php
/**
* @file
* Find and run all Drupal Test Cases as a Test Suite. 
*
* @package DrupalTest
* @author Jonathan H. Wage <jonwage@gmail.com>
* @author Christopher Hopper <christopher.jf.hopper@gmail.com>
*/

require_once realpath(dirname(__FILE__) . '/..') . '/phpunit/Initialize.php';

/**
* All Drupal Tests
*
* This test suite will find all Drupal modules that have test suites 
* named *AllTests.php and will add it to this suite to be executed. 
* Individual module suites can be run directly with the phpunit command. 
*
* @package DrupalTest
* @author Jonathan H. Wage <jonwage@gmail.com>
* @author Christopher Hopper <christopher.jf.hopper@gmail.com>
*/
class AllTests {
  public static function suite() {
    $suite = new DrupalTest_TestSuite('All Drupal Tests');

    $path = realpath(dirname(__FILE__) . '/..') . '/*/tests/*AllTests.php';

    $moduleSuitePaths = glob($path);

    foreach ($moduleSuitePaths as $suitePath) {
      require_once $suitePath;
      // Separate out the component parts of the AllTests code file. 
      $info = pathinfo($suitePath);
      
      // Get the class name of the AllTests class using the file name 
      // of the code file minus the extension. 
      // Note: This is why the naming convention for the code files of 
      // classes is important. 
      $class_name = $info['filename'];

      $suite->addTest(call_user_func(array($class_name, 'suite')));
    }

    return $suite;
  }
}
 