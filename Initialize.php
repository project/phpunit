<?php
/**
* @file
* Initialize testing environment
*
* This script will ensure all files are loaded and the test database 
* environment is prepared. Each test at the minimum should have this 
* file loaded. 
*
* @package DrupalTest
* @author  Jonathan H. Wage <jonwage@gmail.com>
* @author  Christopher Hopper <christopher.jf.hopper@gmail.com>
*/

require_once 'PHPUnit/Framework.php';
require_once 'PHPUnit/TextUI/TestRunner.php';
require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

/** 
* @todo Check on status of MockBrowserTestCase development. Still in 
*   development as at 12 Sep 2008.
* 
* @link http://www.phpunit.de/ticket/459 
*   Ticket #459 (Improved support for Functional Testing) – PHPUnit – Trac
*/
//require_once 'PHPUnit/Extensions/MockBrowserTestCase.php';
//require_once 'MockBrowserTestCase.php';

/**
* Register __autoload function for DrupalTest class includes.
*/
require_once dirname(__FILE__) . '/lib/DrupalTest.php';
spl_autoload_register(array('DrupalTest', 'autoload'));

/**
* Initialize the DrupalTest bootstrap process
*/
DrupalTest_Bootstrap::getInstance()->run();
