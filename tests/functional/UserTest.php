<?php
require_once(realpath(dirname(__FILE__) . '/../../..') . '/phpunit/Initialize.php');

/**
 * User module functional tests
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class UserTest extends DrupalTest_MockBrowserTestCase
{
  public function testUserRegistration()
  {
    // Set user registration to "Visitors can create accounts and no administrator approval is required."
    variable_set('user_register', 1);

    $name = uniqid() . '_test_user';
    $mail = $name .'@example.com';

    $this->get($this->getUrl('?q=user/register'));
    $this->setParameter('name', $name);
    $this->setParameter('mail', $mail);
    $this->submitForm('Create new account');

    // Check database for created user.
    $user = user_load(array('name' => $name, 'mail' => $mail));
    $this->assertTrue($user->uid > 0);

    // Check user fields.
    $this->assertEquals($user->name, $name);
    $this->assertEquals($user->mail, $mail);
    $this->assertEquals($user->mode, 0);
    $this->assertEquals($user->sort, 0);
    $this->assertEquals($user->threshold, 0);
    $this->assertEquals($user->theme, '');
    $this->assertEquals($user->signature, '');
    $this->assertTrue(($user->created > time() - 20 ), 0);
    $this->assertEquals($user->status, variable_get('user_register', 1) == 1 ? 1:0);
    $this->assertEquals($user->timezone, variable_get('date_default_timezone', NULL));
    $this->assertEquals($user->language, '');
    $this->assertEquals($user->picture, '');
    $this->assertEquals($user->init, $mail);
  }
}