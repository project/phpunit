<?php
require_once(realpath(dirname(__FILE__) . '/../../..') . '/phpunit/Initialize.php');

/**
 * Sample mock browser test class
 *
 * This class represents a simple mock browser test case for Drupal
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class SampleMockBrowserTest extends DrupalTest_MockBrowserTestCase
{
    public function testIndexOfNewInstall()
    {
        $this->get($this->getUrl('index.php'));
        $this->assertResponseCodeEquals(200);
        $this->assertTitleEquals('Drupal');
        $this->assertResponseContains('Configure your website');
        $this->assertResponseCodeIsNotError();
    }

    public function testFailedLogin()
    {
        $params = array('op'        => 'Log In',
                        'name'      => '',
                        'pass'      => '',
                        'form_id'   => 'user_login_block');

        $this->post($this->getUrl('index.php?q=node&destination=node'), $params);
        $this->assertResponseCodeEquals(200);
        $this->assertResponseContains('Username field is required.');
        $this->assertResponseContains('Password field is required.');
    }

    public function testLoginByPostingToUrl()
    {
        $params = array('op'        => 'Log In',
                        'name'      => 'testuser',
                        'pass'      => 'password',
                        'form_id'   => 'user_login_block');

        $this->post($this->getUrl('index.php?q=node&destination=node'), $params);
        $this->assertResponseContains('My account');
        $this->assertCurrentUrlEquals($this->getUrl('?q=node'));
    }

    public function testLoginBySubmittingForm()
    {
        $this->get($this->getUrl('index.php'));
        $this->setParameter('name', 'testuser');
        $this->setParameter('pass', 'password');
        $this->submitForm('Log in');
        $this->assertResponseContains('My account');
    }

    public function testGoBack()
    {
        $this->get($this->getUrl('index.php'));
        $this->setParameter('name', 'testuser');
        $this->setParameter('pass', 'password');
        $this->submitForm('Log in');
        $this->assertResponseContains('Welcome to your new Drupal website!');
        $this->clickLink('Administer');
        $this->assertResponseContains('Welcome to the administration section.');
        $this->goBack();
        $this->assertResponseContains('Welcome to your new Drupal website!');
        $this->goForward();
        $this->assertResponseContains('Welcome to the administration section.');
    }

    public function testBrowserRefresh()
    {
        $this->get($this->getUrl('index.php'));
        $this->refresh();
        $this->assertResponseContains('Welcome to your new Drupal website!');
    }
}