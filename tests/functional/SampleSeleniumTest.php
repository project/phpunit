<?php
require_once(realpath(dirname(__FILE__) . '/../../..') . '/phpunit/Initialize.php');

/**
 * Sample selenium test class
 *
 * This class represents a simple selenium test case for Drupal
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class SampleSeleniumTest extends DrupalTest_SeleniumTestCase
{
  public function testSampleTest()
  {
    $this->assertEquals(0, 0);
  }
}