<?php
if ( ! defined('PHPUnit_MAIN_METHOD')) {
    define('PHPUnit_MAIN_METHOD', 'AllSampleTests::main');
}

require_once realpath(dirname(__FILE__) . '/../..') . '/phpunit/Initialize.php';
require_once 'unit/SampleUnitTest.php';
require_once 'functional/SampleSeleniumTest.php';
require_once 'functional/SampleMockBrowserTest.php';
require_once 'functional/UserTest.php';
require_once 'unit/UserUnitTest.php';

/**
 * All Sample phpunit tests
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class AllSampleTests
{
    public static function main()
    {
        PHPUnit_TextUI_TestRunner::run(self::suite());
    }

    public static function suite()
    {
        $suite = new DrupalTest_TestSuite('All Drupal Tests');
        $suite->addTestSuite('SampleUnitTest');
        $suite->addTestSuite('SampleSeleniumTest');
        $suite->addTestSuite('SampleMockBrowserTest');
        $suite->addTestSuite('UserTest');
        $suite->addTestSuite('UserUnitTest');

        return $suite;
    }
}

if (PHPUnit_MAIN_METHOD == 'AllSampleTests::main') {
    AllSampleTests::main();
}