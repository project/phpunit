<?php
require_once(realpath(dirname(__FILE__) . '/../../..') . '/phpunit/Initialize.php');

/**
 * User module unit tests
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class UserUnitTest extends DrupalTest_TestCase
{
  public function testMinLengthName() {
    $name = '';
    $result = user_validate_name($name);
    $this->assertNotNull($result, 'Excessively short username');
  }

  public function testValidCharsName() {
    $name = 'ab/';
    $result = user_validate_name($name);
    $this->assertNotNull($result, 'Invalid chars in username');
  }

  public function testMaxLengthName() {
    $name = str_repeat('a', 61);
    $result = user_validate_name($name);
    $this->assertNotNull($result, 'Excessively long username');
  }

  public function testValidName() {
    $name = 'abc';
    $result = user_validate_name($name);
    $this->assertNull($result, 'Valid username');
  }

  // Mail validation.
  public function testMinLengthMail() {
    $name = '';
    $result = user_validate_mail($name);
    $this->assertNotNull($result, 'Empty mail');
  }

  public function testInValidMail() {
    $name = 'abc';
    $result = user_validate_mail($name);
    $this->assertNotNull($result, 'Invalid mail');
  }

  public function testValidMail() {
    $name = 'absdsdsdc@dsdsde.com';
    $result = user_validate_mail($name);
    $this->assertNull($result, 'Valid mail');
  }
}