<?php
require_once(realpath(dirname(__FILE__) . '/../../..') . '/phpunit/Initialize.php');

/**
 * Sample unit test class
 *
 * This class represents a simple unit test case for Drupal
 *
 * @package DrupalTest
 * @author  Jonathan H. Wage <jonwage@gmail.com>
 */
class SampleUnitTest extends DrupalTest_TestCase
{
  public function testSampleTest()
  {
    $this->assertEquals(0, 0);
  }
}